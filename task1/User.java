package task1;

public class User implements Comparable<User>{

    private static int lastUser = 0;
    private int id;
    private String name;

    public User(){
        this.id = ++lastUser;
        this.name = "user" + id;
    }

    public User(String name){
        this.id = ++lastUser;
        this.name = name;
    }

    public int getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public void setId(int id){
        this.id = id;
    }

    public void setName(String name){
        this.name = name;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(Object user) {
        return (this.hashCode() == ((User)user).hashCode());
    }

    @Override
    public String toString() {
        return ("Id - " + id + " " + "User - " + name);
    }

    @Override
    public int compareTo(User user) {
        return this.name.compareTo(user.getName());
    }
}
