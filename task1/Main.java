package task1;

public class Main {
    public static void main(String[] args) {

        //---List---
        UsersListBox ulb = new UsersListBox();

        ulb.addUser();
        ulb.addUser("Олег");
        ulb.addUser("Егор");
        ulb.addUser("Влад");
        ulb.addUser("Вадим");
        ulb.addUser("Алена");
        ulb.addUser();

        System.out.println(ulb.showUsers());

        ulb.deleteMinUser();
        ulb.deleteMaxUser();
        ulb.deleteLowerUser("Влад");

        System.out.println(ulb.showUsers());
        System.out.println(ulb.sumIdUsers() + " | " + ulb.sumNameUsers());
        System.out.println(ulb.returnUsers(1));
        System.out.println(ulb.returnUsers(1, 3));
        System.out.println(ulb.selectNameUsers("В"));
        System.out.println(ulb.searchSymbolInNameUsers('а'));

        ulb.addInNameUsers("_1");
        System.out.println(ulb.showUsers());

        ulb.sortUsers();
        System.out.println(ulb.showUsers());
        System.out.println("-------------------------------");

        //---Map---
        UsersMapBox umb = new UsersMapBox();

        umb.addUser();
        umb.addUser("Вадим");
        umb.addUser("Алена");
        umb.addUser("Егор");
        umb.addUser("Олег");
        umb.addUser("Влад");
        umb.addUser();

        System.out.println(umb.showUsers());

        umb.deleteMinUser();
        umb.deleteMaxUser();
        umb.deleteLowerUser(4);

        System.out.println(umb.showUsers());
        System.out.println(umb.returnUsers(5));

        System.out.println("-------------------------------");

        //---StreamList---
        StreamUsersListBox sulb = new StreamUsersListBox();

        sulb.addUser();
        sulb.addUser("Олег");
        sulb.addUser("Егор");
        sulb.addUser("Вадим");
        sulb.addUser("Алена");
        sulb.addUser("Влад");
        sulb.showUsers();
        
        sulb.deleteLowerUser(21);
        sulb.showUsers();
        System.out.println();

        sulb.sortUsers();
        sulb.showUsers();
        System.out.println(sulb.sumIdUsers());
        System.out.println(sulb.returnUsers(22));
        System.out.println(sulb.returnUsers(21, 23));
        System.out.println(sulb.selectNameUsers("им"));
    }
}
