package task1;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.*;

public class StreamUsersListBox {

    private List<User> list;

    StreamUsersListBox(){
        this.list = new ArrayList<>();
    }

    public void addUser(){
        list.add(new User());
    }

    public void addUser(String name){
        list.add(new User(name));
    }

    public void deleteLowerUser(int id){
        list = list.stream().filter(user -> user.getId() >= id).collect(Collectors.toList());
    }

    public int sumIdUsers(){
        return list.stream().mapToInt(User::getId).sum();
    }

    public List<User> returnUsers(int index){
        return list.stream().filter(user -> user.getId() == index).collect(Collectors.toList());
    }

    public List<User> returnUsers(int firsIndex, int lastIndex){
        return list.stream().filter(user -> user.getId() > firsIndex && user.getId() <= lastIndex)
                .collect(Collectors.toList());
    }

    public List<User> selectNameUsers(String str){
        return list.stream().filter(user -> user.getName().contains(str)).collect(Collectors.toList());
    }

    public void sortUsers(){
        list = list.stream().sorted(User::compareTo).collect(Collectors.toList());
    }

    public void showUsers(){
        list.stream().forEach(user -> System.out.println(user));
    }
}
