package task1;

import java.util.*;

public class UsersMapBox {

    private Map<UserKey, User> map;

    public UsersMapBox(){
        this.map = new HashMap<>();
    }

    public void addUser(){
        map.put(new UserKey(), new User());
    }

    public void addUser(String name){
        map.put(new UserKey(), new User(name));
    }

    public void deleteMinUser(){
        UserKey minKey = new UserKey();
        for(UserKey key: map.keySet()){
            if(minKey.hashCode() > key.hashCode()){
                minKey = key;
            }
        }
        map.remove(minKey);
    }

    public void deleteMaxUser(){
        UserKey maxKey = new UserKey();
        maxKey.setKey(0);
        for(UserKey key: map.keySet()){
            if(maxKey.hashCode() < key.hashCode()){
                maxKey = key;
            }
        }
        map.remove(maxKey);
    }

    public void deleteLowerUser(int key){
        UserKey userKey = new UserKey();
        while (key > 0){
            userKey.setKey(--key);
            map.remove(userKey);
        }
    }

    public String returnUsers(int key){
        boolean rez = false;
        UserKey rezKey = new UserKey();
        rezKey.setKey(key);
        for(UserKey userKey: map.keySet()){
            if(rezKey.equals(userKey)){
                rez = true;
                rezKey = userKey;
            }
        }
        if(rez){
            return "[ " + key + " ] - " + map.get(rezKey);
        }else {
            return "Invalid key!";
        }
    }

    public String showUsers(){
        String str = "";
        for (Map.Entry<UserKey, User> allUsers: map.entrySet()){
            str += allUsers.getKey().toString() + " | " + allUsers.getValue().toString() + "\n";
        }
        return str;
    }
}
