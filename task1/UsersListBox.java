package task1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UsersListBox {

    private List<User> list;

    UsersListBox(){
        this.list = new ArrayList<>();
    }

    public void addUser(){
        list.add(new User());
    }

    public void addUser(String name){
        list.add(new User(name));
    }

    public void deleteMinUser(){
        User minUser = new User();
        for(User user: list){
            if(minUser.hashCode() > user.hashCode()){
                minUser = user;
            }
        }
        list.remove(minUser);
    }

    public void deleteMaxUser(){
        User maxUser = new User();
        maxUser.setId(0);
        for(User user: list){
            if(maxUser.hashCode() < user.hashCode()){
                maxUser = user;
            }
        }
        list.remove(maxUser);
    }

    public void deleteLowerUser(int id){
        User user = new User();
        while (id > 0){
            user.setId(--id);
            list.remove(user);
        }
    }

    public void deleteLowerUser(String name){
        int id = 0;
        for(User user: list){
            if(name.equals(user.getName())){
                id = user.getId();
            }
        }
        //-------------------------
        if(id > 0){
            User user = new User();
            while (id > 0){
                user.setId(--id);
                list.remove(user);
            }
        }
    }

    public int sumIdUsers(){
        int id = 0;
        for(User user: list){
            id += user.getId();
        }
        return id;
    }

    public String sumNameUsers(){
        String str = "";
        for (User user: list){
            str += user.getName() + "; ";
        }
        return "[ " + str + "]";
    }

    public String returnUsers(int index){
        if(checkIndex(index)){
            User user = list.get(index);
            return user.toString();
        }else {
            return "Invalid index!";
        }
    }

    public String returnUsers(int firsIndex, int lastIndex){
        if(checkIndex(firsIndex, lastIndex)){
            String str = "";
            for(int i = firsIndex; i < lastIndex; i++){
                str += list.get(i).toString() + " ";
            }
            return str;
        }else {
            return "Invalid range!";
        }
    }

    private boolean checkIndex(int index){
        if(index >= 0 && index < list.size()){
            return true;
        }
        return false;
    }

    private boolean checkIndex(int firsIndex, int lastIndex){
        if(!checkIndex(lastIndex)){
            lastIndex = list.size();
        }
        //-------------------------
        if(firsIndex >= 0 && firsIndex < lastIndex){
            return true;
        }else {
            return false;
        }
    }

    public List<User> selectNameUsers(String str){
        List<User> rezultList = new ArrayList<>();
        for (User user: list){
            if(user.getName().contains(str)){
                rezultList.add(user);
            }
        }
        return rezultList;
    }

    public boolean searchSymbolInNameUsers(char symbol){
        boolean rezultSearch = false;
        for(User user: list){
            for(int i = 0; i < user.getName().length(); i++){
                if(symbol == user.getName().charAt(i)){
                    rezultSearch = true;
                    break;
                }
                rezultSearch = false;
            }
        }
        return rezultSearch;

    }

    public void addInNameUsers(String str){
        for(int i = 0; i < list.size(); i++){
            list.get(i).setName(list.get(i).getName() + str);
        }
    }

    public String showUsers(){
        String str = "";
        for (User user: list){
            str += user.toString() + "\n";
        }
        return str;
    }

    public void sortUsers(){
        Collections.sort(list);
    }
}
