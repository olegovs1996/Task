package task1;

public class UserKey {

    private static int number = 0;
    private int key;

    public UserKey(){
        this.key = ++number;
    }

    public int getKey(){
        return key;
    }

    public void setKey(int key){
        this.key = key;
    }

    @Override
    public int hashCode() {
        return key;
    }

    @Override
    public boolean equals(Object userKey) {
        return (this.hashCode() == ((UserKey)userKey).hashCode());
    }

    @Override
    public String toString() {
        return ("Key = " + key);
    }
}
